#coding=utf-8
import os
import cv2
import csv
import numpy as np
import pickle
from keras_frcnn import config
from keras import backend as K
from keras.layers import Input
from keras.models import Model
from keras_frcnn import roi_helpers
import keras_frcnn.resnet as nn
import product_image

class Faster_RCNN:
    def __init__(self, pickle_path):

        with open(pickle_path, 'rb') as f_in:
            self.C = pickle.load(f_in)

        self.C.rot = False
        self.class_mapping = self.C.class_mapping

        if 'bg' not in self.class_mapping:
            self.class_mapping['bg'] = len(self.class_mapping)
            
        self.class_mapping = {v: k for k, v in self.class_mapping.items()}
        self.num_features = 1024

        if K.image_dim_ordering() == 'th':
            self.input_shape_img = (3, None, None)
            self.input_shape_features = (self.num_features, None, None)
        else:
            self.input_shape_img = (None, None, 3)
            self.input_shape_features = (None, None, self.num_features)


        self.img_input = Input(shape = self.input_shape_img)
        self.roi_input = Input(shape=(self.C.num_rois, 4))
        self.feature_map_input = Input(shape = self.input_shape_features)

        # define the base network (resnet here, can be VGG, Inception, etc)
        self.shared_layers = nn.nn_base(self.img_input, trainable=True)

        # define the RPN, built on the base layers
        self.num_anchors = len(self.C.anchor_box_scales) * len(self.C.anchor_box_ratios)
        self.rpn_layers = nn.rpn(self.shared_layers, self.num_anchors)
        self.classifier = nn.classifier(self.feature_map_input, self.roi_input, self.C.num_rois, nb_classes = len(self.class_mapping), trainable=True)

        self.model_rpn = Model(self.img_input, self.rpn_layers)
        self.model_classifier = Model([self.feature_map_input, self.roi_input], self.classifier)
        self.model_rpn.load_weights(self.C.model_path, by_name=True)
        self.model_classifier.load_weights(self.C.model_path, by_name=True)
        self.model_rpn.compile(optimizer='sgd', loss='mse')
        self.model_classifier.compile(optimizer='sgd', loss='mse')
        self.bbox_threshold = 0.8
        print('load frcnn success')

    def format_img_size(self, img, C):
        """ formats the image size based on config """
        img_min_side = float(C.im_size)
        (height,width,_) = img.shape
            
        if width <= height:
            ratio = img_min_side/width
            new_height = int(ratio * height)
            new_width = int(img_min_side)
        else:
            ratio = img_min_side/height
            new_width = int(ratio * width)
            new_height = int(img_min_side)
        img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
        return img, ratio	

    def format_img_channels(self, img, C):
        """ formats the image channels based on config """
        img = img[:, :, (2, 1, 0)]
        img = img.astype(np.float32)
        img[:, :, 0] -= C.img_channel_mean[0]
        img[:, :, 1] -= C.img_channel_mean[1]
        img[:, :, 2] -= C.img_channel_mean[2]
        img /= self.C.img_scaling_factor
        img = np.transpose(img, (2, 0, 1))
        img = np.expand_dims(img, axis=0)
        return img

    def format_img(self, img, C):
        """ formats an image for model prediction based on config """
        img, ratio = self.format_img_size(img, C)
        img = self.format_img_channels(img, C)
        return img, ratio

    # Method to transform the coordinates of the bounding box to its original size
    def get_real_coordinates(self, ratio, x1, y1, x2, y2):

        real_x1 = int(round(x1 // ratio))
        real_y1 = int(round(y1 // ratio))
        real_x2 = int(round(x2 // ratio))
        real_y2 = int(round(y2 // ratio))

        return (real_x1, real_y1, real_x2 ,real_y2)

    def get_frcnn_rect(self, img):
        bboxes = {}
        probs = {}
        product_image_list = []
        X, ratio = self.format_img(img, self.C)
        if K.image_dim_ordering() == 'tf':
            X = np.transpose(X, (0, 2, 3, 1))

        # get the feature maps and output from the RPN
        [Y1, Y2, F] = self.model_rpn.predict(X)

        R = roi_helpers.rpn_to_roi(Y1, Y2, self.C, K.image_dim_ordering(), overlap_thresh=0.5, max_boxes=100)

        # convert from (x1,y1,x2,y2) to (x,y,w,h)
        R[:, 2] -= R[:, 0]
        R[:, 3] -= R[:, 1]

        # apply the spatial pyramid pooling to the proposed regions
    
        for jk in range(R.shape[0]//self.C.num_rois + 1):
            ROIs = np.expand_dims(R[self.C.num_rois*jk:self.C.num_rois*(jk+1), :], axis=0)
            if ROIs.shape[1] == 0:
                break

            if jk == R.shape[0]//self.C.num_rois:
                curr_shape = ROIs.shape
                target_shape = (curr_shape[0], self.C.num_rois, curr_shape[2])
                ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
                ROIs_padded[:, :curr_shape[1], :] = ROIs
                ROIs_padded[0, curr_shape[1]:, :] = ROIs[0, 0, :]
                ROIs = ROIs_padded

            [P_cls, P_regr] = self.model_classifier.predict([F, ROIs])

            for ii in range(P_cls.shape[1]):

                if np.max(P_cls[0, ii, :]) < self.bbox_threshold or np.argmax(P_cls[0, ii, :]) == (P_cls.shape[2] - 1):
                    continue

                cls_name = self.class_mapping[np.argmax(P_cls[0, ii, :])]

                if cls_name not in bboxes:
                    bboxes[cls_name] = []
                    probs[cls_name] = []

                (x, y, w, h) = ROIs[0, ii, :]

                cls_num = np.argmax(P_cls[0, ii, :])
                try:
                    (tx, ty, tw, th) = P_regr[0, ii, 4*cls_num:4*(cls_num+1)]
                    tx /= self.C.classifier_regr_std[0]
                    ty /= self.C.classifier_regr_std[1]
                    tw /= self.C.classifier_regr_std[2]
                    th /= self.C.classifier_regr_std[3]
                    x, y, w, h = roi_helpers.apply_regr(x, y, w, h, tx, ty, tw, th)
                except:
                    pass

                bboxes[cls_name].append([self.C.rpn_stride*x, self.C.rpn_stride*y, self.C.rpn_stride*(x+w), self.C.rpn_stride*(y+h)])
                probs[cls_name].append(np.max(P_cls[0, ii, :]))


        for key in bboxes:
            
            bbox = np.array(bboxes[key])
            new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, np.array(probs[key]), overlap_thresh=0.5)
        
            for jk in range(new_boxes.shape[0]):
                (x1, y1, x2, y2) = new_boxes[jk,:]

                (real_x1, real_y1, real_x2, real_y2) = self.get_real_coordinates(ratio, x1, y1, x2, y2)
                
                if real_x1 < 0:
                    real_x1 = 0
                if real_y1 < 0:
                    real_y1 = 0
                if real_x2 > img.shape [1]:
                    real_x2 = img.shape [1]
                if real_y2 > img.shape[0]:
                    real_y2 = img.shape[0]
                
                seg_img = img[real_y1:real_y2, real_x1:real_x2]

                productimage = product_image.ProductImage(seg_img, ((real_x1,real_y1),(real_x2,real_y2)))
                product_image_list.append(productimage)

                #cv2.rectangle(img,(real_x1, real_y1), (real_x2, real_y2), (0, 0, 255), 3)
        return product_image_list


if __name__ == '__main__':
    faster_rcnn = Faster_RCNN('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/model/config_non_rigid_items_cat7_1584imgs_rpn_max6_20190703.pickle')
    print('load model success')
    img = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/frcnn_result/0_27.jpg')
    print(faster_rcnn.get_frcnn_rect(img))