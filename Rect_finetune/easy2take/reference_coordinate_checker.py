import re

class Reference_Coordinate_Checker:

    def __init__(self, csv_path):
        self.reference_rect_list = []
        self.run(csv_path)
        
    def run(self, csv_path):
        with open(csv_path, 'r') as f:
            for line in f:			
                filename, x1, y1, x2, y2, class_name = self.CSV_reader(line)
                if x1 < 0:
                    x1 = 0
                if y1 < 0:
                    y1 = 0
                if x2 > 1280:
                    x2 = 1280
                if y2 > 720:
                    y2 = 720
                reference_rect = Reference_Rect(((x1,y1),(x2,y2)), filename, class_name)
                self.reference_rect_list.append(reference_rect)
                
    def CSV_reader(self, line):
        x = []
        y = []
        coordinate = []
        line = line.strip().split('"')
        for c in re.split('\[|,|\]| ',line[1]):
            if c != '':
                coordinate.append(int(c))
        
        for idx, c in enumerate(coordinate):

            if idx%2 == 0:
                x.append(c)
            else:
                y.append(c)
        filename = line[0][:-1]
        x1 = min(x)
        x2 = max(x)
        y1 = min(y)
        y2 = max(y)
        class_name = line[2].split(',')[1]
        return filename, x1, y1, x2, y2, class_name




class Reference_Rect:

    def __init__(self, coordinate, filename, class_name):
        self._filename = filename
        self._class_name = class_name
        self._rect_length = self.get_rect_length(coordinate)
        self._rect_width = self.get_rect_width(coordinate)
        self._rect_area = self._rect_length * self._rect_width
        self._rect_center = self.get_rect_center(coordinate)

    @property
    def rect_length(self):
	    return self._rect_length

    @property
    def rect_width(self):
		return self._rect_width

    @property
    def rect_area(self):
		return self._rect_area

    @property
    def rect_center(self):
		return self._rect_center

    def get_rect_length(self, coordinate):
        return coordinate[0][1] - coordinate[0][0]

    def get_rect_width(self, coordinate):
        return coordinate[1][1] - coordinate[1][0]       
    
    def get_rect_center(self, coodinate):
       return (coodinate[0][0]+coodinate[1][0])/2, ((coodinate[0][1]+coodinate[1][1])/2)




if __name__ == '__main__':
    reference_coordinate_checker = Reference_Coordinate_Checker('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/data/frcnn_1500+500.csv')
    print(reference_coordinate_checker.reference_rect_list[0].rect_center)
