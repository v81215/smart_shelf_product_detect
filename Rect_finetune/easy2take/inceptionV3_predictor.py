#coding=utf-8
from keras.models import Model,load_model
import numpy as np
import cv2
import tensorflow as tf

class InceptionV3Predictor:
	def __init__(self, model_path):
		# self.model = load_model(model_path)
		self.model = tf.contrib.keras.models.load_model(model_path)
		print('load inceptionV3 success')
		self.MODE = cv2.BORDER_CONSTANT
		self.COLOR = [0,0,0]
		self.img_size = 299

	def pad_same_as_longside(self, img):
		
		rows, cols, channel = img.shape
		m = max(rows,cols)
		pad_img = cv2.copyMakeBorder(img, (m-rows)/2, m-((m-rows)/2)-rows, (m-cols)/2, m-((m-cols)/2)-cols, 
										self.MODE, value = self.COLOR)
		return pad_img

	def get_predict(self, product_image_list):
		padding_img_list = []
		for product_image in product_image_list:
			img = product_image.product_img.copy()
			img = img.astype('float32')
			img = img/255
			pad_img = self.pad_same_as_longside(img)
			pad_img = cv2.resize(pad_img,(self.img_size, self.img_size))
			padding_img_list.append(pad_img)

		x = np.array(padding_img_list)
		prediction = self.model.predict(x)
		for index, productimage in enumerate(product_image_list):		
			productimage.product_class = prediction[index].argmax()
		
if __name__ == "__main__":
	inceptionv3predictor = InceptionV3Predictor('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/model/shelf_8product_30imgs_aug_result_2019_07_15-InctptionV3-zero-centered-473-0.000000.h5')
	img_list = []
	img = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/frcnn_result/0_1.jpg')
	img_list.append(img)
	img = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/frcnn_result/0_1.jpg')
	img_list.append(img)	
	print(inceptionv3predictor.get_predict(img_list))


