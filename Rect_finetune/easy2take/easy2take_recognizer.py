import cnn_product_detector
import faster_rcnn
import inceptionV3_predictor
import reference_coordinate_checker
import label_checker
import cv2
class Easy2Take_Recongnizer:

    def __init__(self, cnn_model_path, frcnn_pickle_path, inceptionV3_model_path, reference_csv_path, bounding_coordinate):

        self.cnn_product_detector= cnn_product_detector.CnnProductDetector(cnn_model_path, bounding_coordinate) 
        self.faster_rcnn = faster_rcnn.Faster_RCNN(frcnn_pickle_path)
        self.inceptionv3_predictor = inceptionV3_predictor.InceptionV3Predictor(inceptionV3_model_path)
        self.reference_coordinate_checker = reference_coordinate_checker.Reference_Coordinate_Checker(reference_csv_path)
        self.label_checker = label_checker.LabelChecker()

    def get_predict(self, frame):
        label = self.cnn_product_detector.seg_reg(frame)
        product_image_list = self.faster_rcnn.get_frcnn_rect(frame)
        self.label_checker.run(frame, label, self.reference_coordinate_checker.reference_rect_list, product_image_list)
        self.inceptionv3_predictor.get_predict(product_image_list)
        return product_image_list
if __name__ == '__main__':
    
    frame = cv2.imread('img/0_4.jpg')
    cnn_model_path = 'model/recognize_img_rect_28x28_whiteback-084-0.000.h5'
    frcnn_pickle_path = 'model/config_non_rigid_items_cat7_1584imgs_rpn_max6_20190703.pickle'
    inceptionV3_model_path = 'model/shelf_8product_30imgs_aug_result_2019_07_15-InctptionV3-zero-centered-473-0.000000.h5'
    reference_csv_path = 'data/bbox_via_region_data (16).csv'
    bounding_coordinate =  ((240,0),(1280,660))
    easy2take_recongnizer = Easy2Take_Recongnizer(cnn_model_path, frcnn_pickle_path, inceptionV3_model_path, reference_csv_path, bounding_coordinate)
    product_image_list = easy2take_recongnizer.get_predict(frame)
    for product_image in product_image_list:
        print (product_image.product_class)
        print (product_image.product_coordinate)
    