# -*- coding: utf-8 -*-
import os,cv2
from keras.models import Model,load_model
import numpy as np
import tensorflow as tf


class CnnProductDetector:

    def __init__(self, model_path, bounding_coordinate):
        # self.model = load_model(model_path)
        self.model = tf.contrib.keras.models.load_model(model_path)
        self.width = 14
        self.bounding_coordinate = bounding_coordinate
        print('load cnn_product_detect success')

    def bg_detect(self, rect_list):    
        x = np.array(rect_list)
        prediction = self.model.predict(x)
        return prediction

    def seg_reg(self, img):
       
        img = img[self.bounding_coordinate[0][1]:self.bounding_coordinate[1][1], self.bounding_coordinate[0][0]:self.bounding_coordinate[1][0]]    
        img = cv2.resize(img,(int(img.shape[1]/4), int(img.shape[0]/4)))
        img = img[0:(img.shape[0]-img.shape[0]%(self.width*2))+1, 0:(img.shape[1]-img.shape[1]%(self.width*2))+1]
        gray = np.zeros((img.shape[0], img.shape[1]), dtype = np.uint8)
        img = img.astype('float32')
        img = img/255        
        rect_list = []
        
        for x in range(self.width,img.shape[0]-self.width,self.width/2):
            for y in range(self.width,img.shape[1]-self.width,self.width/2):
                
                ret = img[x-self.width:x+self.width,y-self.width:y+self.width]
                rect_list.append(ret)
        prediction = self.bg_detect(rect_list)
        num = 0
        for x in range(self.width,img.shape[0]-self.width,self.width/2):
            for y in range(self.width,img.shape[1]-self.width,self.width/2):
                if prediction[num].argmax() == 0 :
                    gray[x-self.width:x+self.width,y-self.width:y+self.width] = 0
                else:
                    gray[x-self.width:x+self.width,y-self.width:y+self.width] += 20                
                num += 1  
        
        gray = cv2.resize(gray,(int(gray.shape[1]*4),int(gray.shape[0]*4)))
        gray[gray<40]=0 
        gray[gray>0]=255 
        label = np.zeros((720,1280), dtype = np.uint8)
        label[self.bounding_coordinate[0][1]:self.bounding_coordinate[0][1]+gray.shape[0],  self.bounding_coordinate[0][0]:( self.bounding_coordinate[0][0]+gray.shape[1])] = gray
        return label

if __name__ == "__main__":
    img = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/frcnn_result/0_22.jpg')
    cnnproductdetector= CnnProductDetector('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/model/recognize_img_rect_28x28_whiteback-084-0.000.h5', ((240,0),(1280,660)))    
    label = cnnproductdetector.seg_reg(img)
    cv2.imwrite('result.jpg',label)