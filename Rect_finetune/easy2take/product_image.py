
class ProductImage:
	def __init__(self, seg_img, coordinate):
		self._product_class = 0
		self._product_img = seg_img
		self._product_coordinate = coordinate #((x0,y0),(x1,y1))
	
	@property
	def product_img(self):
		return self._product_img

	@property
	def product_coordinate(self):
		return self._product_coordinate

	@property
	def product_class(self):
		return self._product_class

	@product_class.setter
	def product_class(self, class_num):
		self._product_class = class_num