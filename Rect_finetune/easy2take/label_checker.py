from scipy import ndimage
from skimage import measure,morphology
import cv2
import math
import product_image
import numpy as np

class LabelChecker:

    def run(self, frame, label, reference_rect_list, product_image_list):
        label = self.label_combinate(label, product_image_list)
        label = self.remove_small_area(label, reference_rect_list[0].rect_area * 0.3)
        self.find_img_contours(frame, label, reference_rect_list, product_image_list)

    def label_combinate(self, label, product_image_list):
        for product_image in product_image_list:
            real_x1 = product_image.product_coordinate[0][0]
            real_y1 = product_image.product_coordinate[0][1]
            real_x2 = product_image.product_coordinate[1][0]
            real_y2 = product_image.product_coordinate[1][1]
            label[real_y1:real_y2, real_x1:real_x2] = 0	
        return label

    def remove_small_area(self, label, area_thred):
        label, num_features = ndimage.measurements.label(label)
        label = morphology.remove_small_objects(label, min_size =  int(area_thred), connectivity=1)
        label[label>0] = 255
        return label

    def find_img_contours(self, frame, label, reference_rect_list, product_image_list):

        (_,conts,_) = cv2.findContours(label.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        for cnt in conts:
            x, y, l, w = cv2.boundingRect(cnt)
            min_arg = self.find_nearest_reference_rect((x+l/2, y+w/2), reference_rect_list)
            new_l = reference_rect_list[min_arg].rect_length
            new_w = reference_rect_list[min_arg].rect_width
            seg_img = frame[y - int(new_w/2) : y + int(new_w/2), x - int(new_l/2) :  x + int(new_l/2)]            
            productimage = product_image.ProductImage(seg_img, (((x - int(new_l/2)),(y - int(new_w/2))), ((x - int(new_l/2)),(y + int(new_w/2)))))
            product_image_list.append(productimage)

    def find_nearest_reference_rect(self, label_center, reference_rect_list):

        distance_list = []
        for reference_rect in reference_rect_list:
            distance_list.append(self.distance(reference_rect.rect_center, label_center))
        distance_list = np.array(distance_list)
        return np.argmin(distance_list)

    def distance(self, p0, p1):
        return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

    