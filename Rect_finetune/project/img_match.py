#-*- coding: utf-8 -*-
import matplotlib as plt
import cv2
import numpy as np
from skimage import data,segmentation,measure,morphology,color
from scipy import ndimage
from skimage.measure import regionprops,compare_ssim
import script.imageProcessor as imgPcsr
import os
import time 

def img_resize(img):
    img = cv2.resize(img, (int(img.shape[1]/6), int(img.shape[0]/6)), interpolation=cv2.INTER_CUBIC)    
    img = cv2.GaussianBlur(img,(9,9),1)
    return img

def flood_fill(label, height, width):
    result_window_bin, _ = ndimage.measurements.label(label) #取label
    result_window_bin = morphology.remove_small_objects(result_window_bin,min_size=100,connectivity=1) #0是四連通 1是八連通
    result_window_bin = np.insert(result_window_bin, height, 0, axis=0)
    result_window_bin = np.insert(result_window_bin, width, 0, axis=1)
    result_window_bin = np.insert(result_window_bin, 0, 0, axis=0)
    result_window_bin = np.insert(result_window_bin, 0, 0, axis=1)
    result_window_bin = imgPcsr.floodfill(result_window_bin)
    result_window_bin = result_window_bin[1:result_window_bin.shape[0]-1,1:result_window_bin.shape[1]-1]
    result_window_bin[result_window_bin>0] = 255
    
    return result_window_bin

def matchAB(grayA, grayB):    
   
    es = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
    # 转换成灰色
    # 获取图片A的大小
    height, width = grayA.shape
    # 取局部图像，寻找匹配位置
    result_window = np.zeros((height, width), dtype=imgA.dtype)
    for start_y in range(0, height-50, 10):
        for start_x in range(0, width-50, 10):
            window = grayA[start_y:start_y+50, start_x:start_x+50]
            match = cv2.matchTemplate(grayB, window, cv2.TM_CCOEFF_NORMED)
            _, _, _, max_loc = cv2.minMaxLoc(match)
            matched_window = grayB[max_loc[1]:max_loc[1]+50, max_loc[0]:max_loc[0]+50]
            result = cv2.absdiff(window, matched_window)
            result_window[start_y:start_y+50, start_x:start_x+50] = result
 
    # 用四边形圈出不同部分
    _, result_window_bin = cv2.threshold(result_window, 60, 255, cv2.THRESH_BINARY)
    result_window_bin = cv2.dilate(result_window_bin, es, iterations=4) # 形态学膨胀
    result_window_bin = cv2.erode(result_window_bin, es, iterations=4) # 形态学膨胀
    result_window_bin = flood_fill(result_window_bin, height, width)
    cv2.imwrite('flood.jpg',result_window_bin)
      
    return result_window_bin
    
def img_seg_row(img, config):#從config得知座標做分排
    
    row_img_list = []
    for index in range(len(config) - 1):
        seg = img.copy()
        row_img = seg[0:img.shape[0],config[index]:config[index+1]]
        row_img = img_resize(row_img)
        row_img_list.append(row_img)
    return row_img_list

def img_seg_col(dst):#針對單排的ROI做出正矩形切割
    coordinate = []
    _, contours, _ = cv2.findContours(dst.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        dst[y:y+h,0:dst.shape[0]]=255#小二值畫圖
        coordinate.append([x*6,y*6,(x+w)*6,(y+h)*6])#不考慮X座標
    return dst, coordinate
        

def compare_label_img(labelA, labelB, raw_A):
    coordinate = []
    result = cv2.absdiff(labelA, labelB)
    label, _ = ndimage.measurements.label(result) #取label
    print(label.shape[0])
    label = morphology.remove_small_objects(label,min_size=(label.shape[1]*30),connectivity=1) #0是四連通 1是八連通
    label[label>0]=255
    cv2.imwrite('test.jpg',label)
    _, contours, _ = cv2.findContours(label.astype(np.uint8), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        coordinate.append([x,y,x+w,y+h])
    return coordinate

def trun_RGB_channel(img):    
    ch_B = img[:,:,0]
    ch_G = img[:,:,1]
    ch_R = img[:,:,2]
    channel_list = [ch_B, ch_G, ch_R]
    return channel_list
    


class SSIMChecker:

    def check_frame(self, last_frame, current_frame):
        dst0 = self.img_process(last_frame)
        dst1 = self.img_process(current_frame)
        score = compare_ssim(dst0, dst1, full=True)
        return score[0]

    def img_process(self, img):
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
        return hsv[:, :, 2]

        
if __name__ == '__main__':
    config = [273, 580, 855, 1088]
    path = 'raw/'
    folder = os.listdir(path)

    for file in folder:
        
        bg = cv2.imread('bg/0_15.jpg')
        imgA = cv2.imread(path+file)
        time_now = time.time()
        img_count = 0


        imgA = imgA[0:imgA.shape[0]-100,240:imgA.shape[1]] 
        imgA = cv2.resize(imgA, (int(imgA.shape[1]/6), int(imgA.shape[0]/6)), interpolation=cv2.INTER_CUBIC)    
        imgA = cv2.GaussianBlur(imgA,(9,9),1)

        bg = bg[0:bg.shape[0]-100,240:bg.shape[1]]
        bg = cv2.resize(bg, (int(bg.shape[1]/6), int(bg.shape[0]/6)), interpolation=cv2.INTER_CUBIC)    
        bg = cv2.GaussianBlur(bg,(9,9),1)
        

        #imgA_list = img_seg_row(imgA, config)
        #bg_list = img_seg_row(bg, config)
        all_coordinate = []
        #for x in range(1):
        #cv2.imwrite(str(x)+'.jpg',imgA_list[x]) 

        # channel_list_img = trun_RGB_channel(imgA_list[x])
        # channel_list_bg = trun_RGB_channel(bg_list[x])
        # dst = bg_list[x].copy()
        channel_list_img = trun_RGB_channel(imgA)
        channel_list_bg = trun_RGB_channel(bg)
        dst = bg[:,:,0].copy()
        dst[dst>0] = 0
        for y in range(3):#RGB
            label = matchAB(channel_list_bg[y], channel_list_img[y])
            #cv2.imwrite(file.split('.')[0]+'_'+str(y)+'.jpg', label)
            dst[label>0] = 255
        recon_time = time.time()
        process_time = recon_time - time_now
        dst = cv2.resize(dst, ((int(bg.shape[1]*6), int(bg.shape[0]*6))))    
        cv2.imwrite(file, dst)
        print(recon_time - time_now)
            
        #     dst, coordinate = img_seg_col(dst)
        #     for coor in  coordinate:
        #         y1 = coor[1]
        #         y2 = coor[3]
        #         if y2>550:
        #             continue        
        #         print(y1,y2)    
        #         cv2.rectangle(imgA, (config[x],y1), (config[x+1],y2), (0,0,255), 3)
        # cv2.imwrite('result.jpg', imgA)






        #     for index,coor in enumerate(coordinate):
        #         y1 = coor[1]
        #         y2 = coor[3]
        #         for i in range(len(config)-1):
        #             cv2.rectangle(raw_A, (config[i],y1), (config[i+1],y2), (0,0,255), 3)
        # cv2.imwrite(str(x)+'result.jpg', raw_A)
        
                
'''
            #dstB = matchAB(bg_list[x], imgB_list[x])
            
                dstA, minA, maxA = img_seg_col(dstA)
                coordinate = compare_label_img(dstA, dstB, imgA_list[x])
            
            for coor in coordinate:
                x1 = (coor[0]+x*dstA.shape[1])*6
                y1 = coor[1]*6
                x2 = (coor[2]+x*dstA.shape[1])*6
                y2 = coor[3]*6
                if (y2 - y1) > 140:
                    y_num = int((y2 - y1) / 140)  
                    len_y = int((y2 - y1) / y_num)
                    for j in range(y_num):
                        
                        seg_result = raw_A[(y1+len_y*j):(y1+len_y*(j+1)),x1:x2]
                        cv2.imwrite('img'+str(name)+'_'+str(img_count)+'.jpg', seg_result)
                        img_count += 1
                        cv2.rectangle(raw_A, (x1,y1+len_y*j), (x2,y1+len_y*(j+1)), (0,0,255), 3)
                else:
                    cv2.rectangle(raw_A, (x1, y1), (x2, y2), (0, 0, 255), 2)
        cv2.imwrite('result_15.jpg',raw_A)
    #cv2.imwrite('resultB.jpg',raw_B)   
'''
'''
    if abs(maxA[0][1] - maxB[0][1]) > 25:
        cv2.rectangle(raw_A, (0,raw_A.shape[0]), ((x+1)*int(raw_A.shape[1]/3),maxA[0][1]*6), (0,0,255), 3)
        cv2.rectangle(raw_B, (x*int(raw_A.shape[1]/3),maxB[0][1]*6), ((x+1)*int(raw_A.shape[1]/3),maxA[0][1]*6), (0,0,255), 3)
cv2.imwrite('testA.jpg',raw_A)
cv2.imwrite('testB.jpg',raw_B)
'''
'''

cv2.imwrite('dstA.jpg',dstA)
cv2.imwrite('dstB.jpg',dstB)
dstA_list = img_seg_row(dstA, 3)
dstB_list = img_seg_row(dstB, 3)

for x in range(3):

cv2.rectangle(raw_A, (x*int(raw_A.shape[1]/3),maxB[0][1]*6), ((x+1)*int(raw_A.shape[1]/3),maxA[0][1]*6), (0,0,255), 3)
cv2.rectangle(raw_B, (x*int(raw_A.shape[1]/3),maxB[0][1]*6), ((x+1)*int(raw_A.shape[1]/3),maxA[0][1]*6), (0,0,255), 3)
cv2.imwrite('resultA.jpg',raw_A)
cv2.imwrite('resultB.jpg',raw_B)
'''
'''
if (max[0][0]*6-min[0][0]*6) > 290 or (max[0][1]*6-min[0][1]*6) > 140:
x_num = int((max[0][0]*6-min[0][0]*6) / 290)
y_num = int((max[0][1]*6-min[0][1]*6) / 140)
print(x_num, y_num)
coor_xlist = []
coor_ylist = []
len_x = int((max[0][0]*6-min[0][0]*6) / x_num)
len_y = int((max[0][1]*6-min[0][1]*6) / y_num)
for i in range(x_num + 1):
    coordinate = (min[0][0]*6)+len_x*i
    coor_xlist.append(coordinate)
for j in range(y_num + 1):
    coordinate = (min[0][1]*6)+len_y*j
    coor_ylist.append(coordinate)
coordinate = []
for i in range(len(coor_xlist)-1):
    for j in range(len(coor_ylist)-1):
        print(coor_xlist[i],coor_ylist[j],coor_xlist[i+1],coor_ylist[j+1])
        cv2.rectangle(raw_B, (coor_xlist[i],coor_ylist[j]), (coor_xlist[i+1],coor_ylist[j+1]), (0,0,255), 3)
else:
cv2.rectangle(raw_A, (min[0][0]*6,min[0][1]*6), (max[0][0]*6,max[0][1]*6), (0,0,255), 3)
cv2.rectangle(raw_B, (min[0][0]*6,min[0][1]*6), (max[0][0]*6,max[0][1]*6), (0,0,255), 3)
cv2.imwrite('resultA.jpg',raw_A)
cv2.imwrite('resultB.jpg',raw_B)
'''




'''
SSIMchecker = SSIMChecker()
camera = cv2.VideoCapture(0) # 参数0表示第一个摄像头
camera.set(3, 1280)
camera.set(4, 720)
bs = cv2.createBackgroundSubtractorKNN(detectShadows=True)
es = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (3, 3))
count = 5
grabbed, imgA = camera.read()
grabbed, imgB = camera.read()

grabbed, compare = camera.read()
textLabel = ''
list_cam = []
list_score = []
while True:
grabbed, frame = camera.read()
list_cam.append(frame)
if len(list_cam)>2:
    del list_cam[0]
cv2.putText(frame, textLabel, (50,50), cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)
cv2.imshow('frame', frame)
if len(list_cam) == 2 :
    score = SSIMchecker.check_frame(list_cam[0], list_cam[1])
    list_score.append(score)
    del list_cam[0]

    if score < 0.85 :
        textLabel = 'hand in'
        imgA = list_cam[0].copy()
        count = 5
    
    elif score > 0.85 and count >0 :
        textLabel = 'no hand'
        count -= 1
    
    elif score > 0.85 and count == 0:                
        
        imgB = frame.copy()
        raw_B = imgB.copy()
        small_imgB = cv2.resize(imgB, (int(imgB.shape[1]/7), int(imgB.shape[0]/7)), interpolation=cv2.INTER_CUBIC)
        small_imgB = cv2.GaussianBlur(small_imgB,(9,9),1)

        raw_A = imgA.copy()            

        small_imgA = cv2.resize(imgA, (int(imgA.shape[1]/7), int(imgA.shape[0]/7)), interpolation=cv2.INTER_CUBIC)
        small_imgA = cv2.GaussianBlur(small_imgA,(9,9),1)
        
        min, max = matchAB(small_imgA, small_imgB)
        if min != []:
            raw_A = raw_A[min[0][1]*7:max[0][1]*7, min[0][0]*7:max[0][0]*7]
            raw_B = raw_B[min[0][1]*7:max[0][1]*7, min[0][0]*7:max[0][0]*7]
            cv2.imshow('crop_A', raw_A)
            cv2.imshow('crop_B', raw_B)
        count = 5
        
k = cv2.waitKey(10) & 0xff

if k == 27:
    break
'''