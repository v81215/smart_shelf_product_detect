#coding=utf-8
import os
import cv2
import csv
import numpy as np
import sys
import pickle
from optparse import OptionParser
import time
from keras_frcnn import config
from keras import backend as K
from keras.layers import Input
from keras.models import Model
from keras_frcnn import roi_helpers
from datetime import datetime
import threading

import predict_one_by_one_fine_tune
import cnn_background_recognize
import CSV_reader
os.environ["CUDA_VISIBLE_DEVICES"] = '0'
sys.setrecursionlimit(40000)#最大遞迴次數

parser = OptionParser()
parser.add_option("-n", "--num_rois", dest="num_rois", help="Number of ROIs per iteration. Higher means more memory use.", default=32)
parser.add_option("--config_filename", dest="config_filename", help="Location to read the metadata related to the training (generated when training).", default="/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/model/config_non_rigid_items_cat7_1584imgs_rpn_max6_20190703.pickle")
parser.add_option("--network", dest="network", help="Base network to use. Supports vgg or resnet50.", default='resnet50')

(options, args) = parser.parse_args()

config_output_filename = options.config_filename

with open(config_output_filename, 'rb') as f_in:
	C = pickle.load(f_in)

if C.network == 'resnet50':
	import keras_frcnn.resnet as nn
elif C.network == 'vgg':
	import keras_frcnn.vgg as nn

# turn off any data augmentation at test time
C.rot = False
#C.model_path = options.weight_path



class_mapping = C.class_mapping

if 'bg' not in class_mapping:
	class_mapping['bg'] = len(class_mapping)


class_mapping = {v: k for k, v in class_mapping.items()}

print(class_mapping)
class_to_color = {class_mapping[v]: np.random.randint(0, 255, 3) for v in class_mapping}
C.num_rois = int(options.num_rois)

if C.network == 'resnet50':
	num_features = 1024
elif C.network == 'vgg':
	num_features = 512

if K.image_dim_ordering() == 'th':
	input_shape_img = (3, None, None)
	input_shape_features = (num_features, None, None)
else:
	input_shape_img = (None, None, 3)
	input_shape_features = (None, None, num_features)


img_input = Input(shape=input_shape_img)
roi_input = Input(shape=(C.num_rois, 4))
feature_map_input = Input(shape=input_shape_features)

# define the base network (resnet here, can be VGG, Inception, etc)
shared_layers = nn.nn_base(img_input, trainable=True)

# define the RPN, built on the base layers
num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
rpn_layers = nn.rpn(shared_layers, num_anchors)


classifier = nn.classifier(feature_map_input, roi_input, C.num_rois, nb_classes=len(class_mapping), trainable=True)

model_rpn = Model(img_input, rpn_layers)

model_classifier_only = Model([feature_map_input, roi_input], classifier)


model_classifier = Model([feature_map_input, roi_input], classifier)

print('Loading weights from {}'.format(C.model_path))
model_rpn.load_weights(C.model_path, by_name=True)

model_classifier.load_weights(C.model_path, by_name=True)

model_rpn.compile(optimizer='sgd', loss='mse')

model_classifier.compile(optimizer='sgd', loss='mse')

all_imgs = []

classes = {}

bbox_threshold = 0.8

visualise = True

item_name=['Lays','Doritos','Enaak','Squid','Calamuju','Potato chips','Cola','Chicken Noodles','Fast noodles(big)','Fast noodles(small)']

def format_img_size(img, C):
	""" formats the image size based on config """
	img_min_side = float(C.im_size)
	(height,width,_) = img.shape
		
	if width <= height:
		ratio = img_min_side/width
		new_height = int(ratio * height)
		new_width = int(img_min_side)
	else:
		ratio = img_min_side/height
		new_width = int(ratio * width)
		new_height = int(img_min_side)
	img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
	return img, ratio	

def format_img_channels(img, C):
	""" formats the image channels based on config """
	img = img[:, :, (2, 1, 0)]
	img = img.astype(np.float32)
	img[:, :, 0] -= C.img_channel_mean[0]
	img[:, :, 1] -= C.img_channel_mean[1]
	img[:, :, 2] -= C.img_channel_mean[2]
	img /= C.img_scaling_factor
	img = np.transpose(img, (2, 0, 1))
	img = np.expand_dims(img, axis=0)
	return img

def format_img(img, C):
	""" formats an image for model prediction based on config """
	img, ratio = format_img_size(img, C)
	img = format_img_channels(img, C)
	return img, ratio

# Method to transform the coordinates of the bounding box to its original size
def get_real_coordinates(ratio, x1, y1, x2, y2):

	real_x1 = int(round(x1 // ratio))
	real_y1 = int(round(y1 // ratio))
	real_x2 = int(round(x2 // ratio))
	real_y2 = int(round(y2 // ratio))

	return (real_x1, real_y1, real_x2 ,real_y2)



def fasterRCNN_reg(img, label):

	productimage_list = []
	X, ratio = format_img(img, C)
	if K.image_dim_ordering() == 'tf':
		X = np.transpose(X, (0, 2, 3, 1))

	# get the feature maps and output from the RPN
	[Y1, Y2, F] = model_rpn.predict(X)


	R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_dim_ordering(), overlap_thresh=0.5, max_boxes=100)

	# convert from (x1,y1,x2,y2) to (x,y,w,h)
	R[:, 2] -= R[:, 0]
	R[:, 3] -= R[:, 1]

	# apply the spatial pyramid pooling to the proposed regions
	bboxes = {}
	probs = {}

	for jk in range(R.shape[0]//C.num_rois + 1):
		ROIs = np.expand_dims(R[C.num_rois*jk:C.num_rois*(jk+1), :], axis=0)
		if ROIs.shape[1] == 0:
			break

		if jk == R.shape[0]//C.num_rois:
			#pad R
			curr_shape = ROIs.shape
			target_shape = (curr_shape[0],C.num_rois,curr_shape[2])
			ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
			ROIs_padded[:, :curr_shape[1], :] = ROIs
			ROIs_padded[0, curr_shape[1]:, :] = ROIs[0, 0, :]
			ROIs = ROIs_padded

		[P_cls, P_regr] = model_classifier.predict([F, ROIs])

		for ii in range(P_cls.shape[1]):

			if np.max(P_cls[0, ii, :]) < bbox_threshold or np.argmax(P_cls[0, ii, :]) == (P_cls.shape[2] - 1):
				continue

			cls_name = class_mapping[np.argmax(P_cls[0, ii, :])]

			if cls_name not in bboxes:
				bboxes[cls_name] = []
				probs[cls_name] = []

			(x, y, w, h) = ROIs[0, ii, :]

			cls_num = np.argmax(P_cls[0, ii, :])
			try:
				(tx, ty, tw, th) = P_regr[0, ii, 4*cls_num:4*(cls_num+1)]
				tx /= C.classifier_regr_std[0]
				ty /= C.classifier_regr_std[1]
				tw /= C.classifier_regr_std[2]
				th /= C.classifier_regr_std[3]
				x, y, w, h = roi_helpers.apply_regr(x, y, w, h, tx, ty, tw, th)
			except:
				pass

			bboxes[cls_name].append([C.rpn_stride*x, C.rpn_stride*y, C.rpn_stride*(x+w), C.rpn_stride*(y+h)])
			probs[cls_name].append(np.max(P_cls[0, ii, :]))


	for key in bboxes:
		
		bbox = np.array(bboxes[key])
	
		#print 'key:'+key
		#print 'cat:'+cat
		
		#重疊到0.5就認定兩個是同樣的東西 
		new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, np.array(probs[key]), overlap_thresh=0.5)
	
		for jk in range(new_boxes.shape[0]):
			(x1, y1, x2, y2) = new_boxes[jk,:]

			(real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)
			
			if real_x1 < 0:
				real_x1 = 0
			if real_y1 < 0:
				real_y1 = 0
			if real_x2 > img.shape [1]:
				real_x2 = img.shape [1]
			if real_y2 > img.shape[0]:
				real_y2 = img.shape[0]
			
			
			seg_img = img[real_y1:real_y2, real_x1:real_x2]
			label[real_y1:real_y2, real_x1:real_x2] = 0	
			productimage = ProductImage(seg_img, ((real_x1,real_y1),(real_x2, real_y2)))
			productimage_list.append(productimage)
			#cv2.rectangle(img,(real_x1, real_y1), (real_x2, real_y2), (0, 0, 255), 3)
	return productimage_list, label
class webcamCapture:
	def __init__(self,cam_num):
		self.Frame = []
		self.capture = cv2.VideoCapture(cam_num)
		self.capture.set(3,1280)
		self.capture.set(4,720)
	def start(self):
	# 把程式放進子執行緒，daemon=True 表示該執行緒會隨著主執行緒關閉而關閉。
		t1=threading.Thread(target=self.queryframe, args=())
		t1.daemon=True
		t1.start()
	def getframe(self):
		# 當有需要影像時，再回傳最新的影像。
		return self.Frame
	def queryframe(self):
		while 1:
			self.status, self.Frame = self.capture.read()

class ProductImage:
	def __init__(self, seg_img, coordinate):
		self._product_class = 0
		self._product_img = seg_img
		self._product_coordinate = coordinate
	
	@property
	def product_img(self):
		return self._product_img

	@property
	def product_coordinate(self):
		return self._product_coordinate

	@property
	def product_class(self):
		return self._product_class

	@product_class.setter
	def product_class(self, class_num):
		self._product_class = class_num

	


if __name__ == '__main__':
	# webcam = webcamCapture(0)
	# # 啟動子執行緒
	# webcam.start()
	# # 暫停1秒，確保影像已經填充
	# time.sleep(1)
	# 使用無窮迴圈擷取影像，直到按下Esc鍵結束
	img_path = '/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/img/'
	folder = os.listdir(img_path)
	file_num = 0
	reference_coordinate_list = []
	with open('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/frcnn/data/bbox_via_region_data (16).csv','r') as f:
		for line in f:			
			filename, x1, y1, x2, y2, class_name = CSV_reader.parse_line2(line)
			if x1 < 0:
				x1 = 0
			if y1 < 0:
				y1 = 0
			if x2 > 1280:
				x2 = 1280
			if y2 > 720:
				y2 = 720
			if filename == '0_1.jpg':
				reference_coordinate_list.append(((x1,y1),(x2,y2)))
	Result_File = open("20190804_seg+pre_result.csv","w")
	csv_writer = csv.writer(Result_File)
	# while True:
	for file in folder:
		time_now=time.time()		
		# 使用 getframe 取得最新的影像
		# img= webcam.getframe()
		img = cv2.imread(img_path+file)
		label,gray = cnn_background_recognize.seg_reg(img)
		cv2.imwrite(file.split('.')[0] + '_label.jpg', label)
		productimage_list, label = fasterRCNN_reg(img, label)
		
		for index, reference_coordinate in enumerate(reference_coordinate_list):
			compare_img = label[reference_coordinate[0][1]:reference_coordinate[1][1], reference_coordinate[0][0]:reference_coordinate[1][0]]
			if (float(np.sum(compare_img == 255))/float(np.sum(compare_img == 255)+np.sum(compare_img == 0)))>0.3:
				seg_img = img[reference_coordinate[0][1]:reference_coordinate[1][1], reference_coordinate[0][0]:reference_coordinate[1][0]]
				#cv2.rectangle(img,(reference_coordinate[0][0], reference_coordinate[0][1]), (reference_coordinate[1][0], reference_coordinate[1][1]), (0, 255, 0), 3)
				productimage = ProductImage(seg_img, (( reference_coordinate[0][0],reference_coordinate[0][1]),(reference_coordinate[1][0], reference_coordinate[1][1])))
				productimage_list.append(productimage)

		predict_one_by_one_fine_tune.get_predict(productimage_list)
		
		for productimage in productimage_list:
			# cv2.rectangle(img,(productimage.product_coordinate[0][0], productimage.product_coordinate[0][1]), (productimage.product_coordinate[1][0], productimage.product_coordinate[1][1]), (0, 0, 255), 8)
			data = [file, productimage.product_coordinate, str(productimage.product_class)]
			csv_writer.writerow(data)
			textLabel = item_name[productimage.product_class]
			print (item_name[productimage.product_class])
			(retval,baseLine) = cv2.getTextSize(textLabel,cv2.FONT_HERSHEY_COMPLEX,1,1)
			textOrg = (productimage.product_coordinate[1][0]-int(productimage.product_img.shape[1]/2),  productimage.product_coordinate[1][1])

			cv2.rectangle(img, (textOrg[0] - 5, textOrg[1]+baseLine - 5), (textOrg[0]+retval[0] + 5, textOrg[1]-retval[1] - 5), (0, 0, 0), 2)
			cv2.rectangle(img, (textOrg[0] - 5,textOrg[1]+baseLine - 5), (textOrg[0]+retval[0] + 5, textOrg[1]-retval[1] - 5), (255, 255, 255), -1)

			cv2.putText(img, textLabel, textOrg, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)
		

		recon_time=time.time()
		print(recon_time-time_now)
		#cv2.imwrite('smart_shelf_img/test/data50/result/'+str(i)+'.jpg',img)
		cv2.imwrite(file,img)
		# cv2.imshow('Image', img)
		cv2.imwrite(file.split('.')[0]+'_label_frcnn.jpg', label)
		if cv2.waitKey(1) & 0xFF == ord('t'):
			print('take picture')
			cv2.imwrite(folder[file_num]+'.jpg',img)
		if cv2.waitKey(1) & 0xFF == ord('n'):
			file_num += 1
		if cv2.waitKey(1) == 27:
			cv2.destroyAllWindows()
			webcam.stop()
			break
		
