# -*- coding: utf-8 -*-
import IOU_Calculator
import length_cul
import CSV_reader
import re
import numpy as np
from os import listdir
import os
import sys
import matplotlib.pyplot as plt
import cv2
import time
import random
import csv
def CollectData(CSV_name):
	with open(CSV_name,'r') as f:
		dic_coordiate={}
		dic_class={}
		for line in f:			
			filename, x1, y1, x2, y2, class_name = CSV_reader.parse_line2(line)
			if filename not in dic_coordiate:
				dic_coordiate[filename]=[]
				dic_class[filename]=[]
			dic_coordiate[filename].append([x1,y1,x2,y2])
			dic_class[filename].append(class_name)
	
	return dic_coordiate,dic_class



if __name__=="__main__":
	Result_File = open("CNN_result.csv","w")
	FasterRCNN_File = open("FRCNN_without_0.3IOU_result.csv","w")
	w = csv.writer(Result_File)
	w_FasterRCNN_File = csv.writer(FasterRCNN_File)
	FasterRCNN_Name='/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/fasterRCNN/smart_shelf_non_rigid_items_cat7_rpn_max6_20190703_test_bbox_v2.csv'
	FasterRCNN_coordiate,FasterRCNN_class=CollectData(FasterRCNN_Name)
	Ground_Truth_Name =  '/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/ground_truth/bbox_via_region_data (16).csv'
	Ground_Truth_coordiate,Ground_Truth_class=CollectData(Ground_Truth_Name)
	#回傳所有有物的框給frcnn辨識
	'''
	for filename, ground_truth_coordiates in Ground_Truth_coordiate.iteritems():
		img = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/fasterRCNN/test3/'+filename)
		print img
		for ground_truth_coordiate in ground_truth_coordiates:
			
			cv2.rectangle(img, (ground_truth_coordiate[0],ground_truth_coordiate[1]), (ground_truth_coordiate[2],ground_truth_coordiate[3]), (0, 255, 0), 3)#frcnn
		#cv2.imwrite(filename.split('.')[0]+'_ground_truth.jpg', img)
	'''

	for filename, frcnn_coordiates in FasterRCNN_coordiate.iteritems():

		print 'filename', filename
		init_time = time.time()
		miss_detection = []
		segment_coordinates = []
		cnn_recongnize_coordiates, raw = length_cul.lenth_cul_main(filename, frcnn_coordiates)
		frcnn_img = raw.copy()
		cnn_img = raw.copy()
		result_img = raw.copy()
		for frcnn_coordiate in frcnn_coordiates:
			cv2.rectangle(frcnn_img, (frcnn_coordiate[0],frcnn_coordiate[1]), (frcnn_coordiate[2],frcnn_coordiate[3]), (0, 255, 0), 3)#frcnn
		for cnn_recongnize_coordiate in cnn_recongnize_coordiates:
			cv2.rectangle(cnn_img, (cnn_recongnize_coordiate[0],cnn_recongnize_coordiate[1]), (cnn_recongnize_coordiate[2],cnn_recongnize_coordiate[3]), (255, 0, 0), 3)#cnn
			data = filename, cnn_recongnize_coordiate
			w.writerow(data)
		for cnn_num, cnn_recongnize_coordiate in enumerate(cnn_recongnize_coordiates):
		
			all_ratio=[]
			max_ratio=0
			for frcnn_num, frcnn_coordiate in enumerate(frcnn_coordiates):
				ratio,Reframe,GTframe=IOU_Calculator.IOU(cnn_recongnize_coordiate, frcnn_coordiate)
				all_ratio.append(ratio)
			# print all_ratio
			if all_ratio!=[]:
				max_ratio = max(all_ratio)				
				arg_max = np.argmax(all_ratio)
				if max_ratio > 0.3:	
					data = filename, frcnn_coordiate, FasterRCNN_class[filename][arg_max]
					w_FasterRCNN_File.writerow(data)
					segment_coordinates.append(frcnn_coordiates[arg_max])				
					del frcnn_coordiates[arg_max]
					del FasterRCNN_class[filename][arg_max]
				else:
					miss_detection.append(cnn_recongnize_coordiate)
					segment_coordinates.append(cnn_recongnize_coordiate)
		
		# print time.time()-init_time
		# print 'false alarm:', frcnn_coordiates				
		# print 'miss detection', miss_detection
		print 'segment coordinate', segment_coordinates
		print '--------------------------------------------'
		for frcnn_coordiate in frcnn_coordiates:
			cv2.rectangle(raw, (frcnn_coordiate[0],frcnn_coordiate[1]), (frcnn_coordiate[2],frcnn_coordiate[3]), (0, 255, 255), 3)#frcnn
			
		for miss_detect in miss_detection:
			cv2.rectangle(raw, (miss_detect[0],miss_detect[1]), (miss_detect[2],miss_detect[3]), (255, 255, 0), 3)#cnn
		for segment_coordinate in segment_coordinates:
			B = random.randint(0, 255) 
			G = random.randint(0, 255) 
			R = random.randint(0, 255) 
			cv2.rectangle(result_img, (segment_coordinate[0],segment_coordinate[1]), (segment_coordinate[2],segment_coordinate[3]), (B, G, R), 3)#cnn
			
		# cv2.imwrite(filename.split('.')[0]+'_result.jpg', frcnn_img)
		cv2.imwrite('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/test_result/20190716/'+filename.split('.')[0]+'_cnn.jpg', cnn_img)
		# cv2.imwrite(filename.split('.')[0]+'_all.jpg', result_img)
