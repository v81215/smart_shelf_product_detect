# -*- coding: utf-8 -*-
import test11
import CSV_reader
import re
import numpy as np
from os import listdir
import csv
import os
import sys


def sortedDictValues(adict): 
	keys = adict.keys() 
	keys.sort() 
	return map(adict.get, keys) 

def CollectData(CSV_name):
	with open(CSV_name,'r') as f:
		dic_coordiate={}
		dic_class={}
		for line in f:			
			filename, x1, y1, x2, y2, class_name = CSV_reader.parse_line2(line)
			print x1
			if filename.split('.')[0] not in dic_coordiate:
				dic_coordiate[filename.split('.')[0]]=[]
				dic_class[filename.split('.')[0]]=[]
			dic_coordiate[filename.split('.')[0]].append([x1,y1,x2,y2])
			dic_class[filename.split('.')[0]].append(class_name)
	
	return dic_coordiate,dic_class



if __name__=="__main__":
	Result_File = open("/home/n100-2/CNN/mulIOU_count/smart_shelf_20190524/test4/model_cat10_1500+1000imgs_test4_20190527_loss0.157_testimg2_result.csv","w")
	w = csv.writer(Result_File)
	GT_Name='/home/n100-2/CNN/mulIOU_count/smart_shelf_20190524/test4/bbox_non_rigid_proposal_testimg2.csv'
	Test_Name='/home/n100-2/CNN/mulIOU_count/smart_shelf_20190524/test4/model_cat10_1500+1000imgs_test4_20190527_loss0.157_testimg2.csv'
	GT_all_coordiate,GT_all_class=CollectData(GT_Name)
	Test_all_coordiate,Test_all_class=CollectData(Test_Name)
	for key, value in GT_all_coordiate.iteritems():
		print 'filename:',key
		TP=0#框對
		FN=0#Miss detection
		FP=0#False alarm錯誤的肯定
		correct_reg=0#正確辨識
	
		if key in Test_all_coordiate:
			for index,GT_coordiate in enumerate(GT_all_coordiate[key]):
				all_ratio=[]
				max_ratio=0
		
				for Test_coordiate in Test_all_coordiate[key]:
					print GT_coordiate
					print Test_coordiate
					ratio,Reframe,GTframe=test11.IOU(Test_coordiate,GT_coordiate)
					all_ratio.append(ratio)
				#print GT_all_coordiate[key]
				#print Test_all_coordiate[key]
				#print all_ratio
				if all_ratio!=[]:
					max_ratio = max(all_ratio)
		
				if max_ratio>0:
					#print max_ratio
					index_test_max=[i for i, j in enumerate(all_ratio) if j == max_ratio]
					if max_ratio>0.3:
						TP+=1
						#print GT_all_class[key][index]
						#print Test_all_class[key][index_test_max[0]]
						if GT_all_class[key][index]==Test_all_class[key][index_test_max[0]]:
							correct_reg+=1
			
						del Test_all_coordiate[key][index_test_max[0]]
						del Test_all_class[key][index_test_max[0]]
			FP=len(Test_all_coordiate[key])
			FN=len(GT_all_coordiate[key])-TP
		else:
			FN = 1
		#print TP,FN,FP,correct_reg	
		data=key,TP,FN,FP,correct_reg
		w.writerow(data)



'''

				
				
				w.writerow(data)
				result.append(ratio)
				if ratio==0.0:
				count=count+1
		
		else:
			continue
	mean=np.mean(result)
	data2=[[mean],[count]]
	w.writerow(data2)	
f.close()


>>> m = max(a)
>>> [i for i, j in enumerate(a) if j == m]
[9, 12]
'''
