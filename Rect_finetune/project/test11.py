# -*- coding: utf-8 -*-
import numpy as np

'''
a = np.array([[1, 1,1], [2,2, 2], [3,3, 3]])

height, width=a.shape
a=np.insert(a, height, 0, axis=0)
a=np.insert(a, width, 0, axis=1)
a=np.insert(a, 0, 0, axis=0)
a=np.insert(a, 0, 0, axis=1)

a=a[1:height+1, 1:width+1]

print a
'''

"""
Created on Sun Aug 07 14:26:51 2016

@author: Eddy_zheng
"""
def IOU(Reframe,GTframe):
   
	x1 = Reframe[0];
	y1 = Reframe[1];
	width1 = Reframe[2]-Reframe[0];
	height1 = Reframe[3]-Reframe[1];

	x2 = GTframe[0];
	y2 = GTframe[1];
	width2 = GTframe[2]-GTframe[0];
	height2 = GTframe[3]-GTframe[1];

	endx = max(x1+width1,x2+width2);
	startx = min(x1,x2);
	width = width1+width2-(endx-startx);

	endy = max(y1+height1,y2+height2);
	starty = min(y1,y2);
	height = height1+height2-(endy-starty);

	if width <=0 or height <= 0:
		ratio = 0 # 重叠率为 0 
	else:
        	Area = width*height; # 两矩形相交面积
		Area1 = width1*height1; 
		Area2 = width2*height2;
        	ratio = Area*1./(Area1+Area2-Area);
	# return IOU
	return ratio,Reframe,GTframe






