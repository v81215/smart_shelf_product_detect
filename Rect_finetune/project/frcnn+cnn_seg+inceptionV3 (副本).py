#coding=utf-8
from __future__ import division
import os
import cv2
import csv
import numpy as np
import sys
import pickle
from optparse import OptionParser
import time
from keras_frcnn import config
from keras import backend as K
from keras.layers import Input
from keras.models import Model
from keras_frcnn import roi_helpers
from datetime import datetime
import operator
import threading
os.environ["CUDA_VISIBLE_DEVICES"] = '1'
sys.setrecursionlimit(40000)

parser = OptionParser()

parser.add_option("-n", "--num_rois", dest="num_rois", help="Number of ROIs per iteration. Higher means more memory use.", default=32)
parser.add_option("--config_filename", dest="config_filename", help="Location to read the metadata related to the training (generated when training).", default="/home/n100-2/CNN/frcnn/config_smart_shelf_cat10_height40_5000imgs_20190516.pickle")
parser.add_option("--network", dest="network", help="Base network to use. Supports vgg or resnet50.", default='resnet50')

(options, args) = parser.parse_args()

config_output_filename = options.config_filename

with open(config_output_filename, 'rb') as f_in:
	C = pickle.load(f_in)

if C.network == 'resnet50':
	import keras_frcnn.resnet as nn
elif C.network == 'vgg':
	import keras_frcnn.vgg as nn

# turn off any data augmentation at test time
C.rot = False
#C.model_path = options.weight_path


def format_img_size(img, C):
	""" formats the image size based on config """
	img_min_side = float(C.im_size)
	(height,width,_) = img.shape
		
	if width <= height:
		ratio = img_min_side/width
		new_height = int(ratio * height)
		new_width = int(img_min_side)
	else:
		ratio = img_min_side/height
		new_width = int(ratio * width)
		new_height = int(img_min_side)
	img = cv2.resize(img, (new_width, new_height), interpolation=cv2.INTER_CUBIC)
	return img, ratio	

def format_img_channels(img, C):
	""" formats the image channels based on config """
	img = img[:, :, (2, 1, 0)]
	img = img.astype(np.float32)
	img[:, :, 0] -= C.img_channel_mean[0]
	img[:, :, 1] -= C.img_channel_mean[1]
	img[:, :, 2] -= C.img_channel_mean[2]
	img /= C.img_scaling_factor
	img = np.transpose(img, (2, 0, 1))
	img = np.expand_dims(img, axis=0)
	return img

def format_img(img, C):
	""" formats an image for model prediction based on config """
	img, ratio = format_img_size(img, C)
	img = format_img_channels(img, C)
	return img, ratio

# Method to transform the coordinates of the bounding box to its original size
def get_real_coordinates(ratio, x1, y1, x2, y2):

	real_x1 = int(round(x1 // ratio))
	real_y1 = int(round(y1 // ratio))
	real_x2 = int(round(x2 // ratio))
	real_y2 = int(round(y2 // ratio))

	return (real_x1, real_y1, real_x2 ,real_y2)

def get_category(str):
	cat = str.split('_')[0]
	return cat


class_mapping = C.class_mapping

if 'bg' not in class_mapping:
	class_mapping['bg'] = len(class_mapping)


class_mapping = {v: k for k, v in class_mapping.items()}

print(class_mapping)
class_to_color = {class_mapping[v]: np.random.randint(0, 255, 3) for v in class_mapping}
C.num_rois = int(options.num_rois)

if C.network == 'resnet50':
	num_features = 1024
elif C.network == 'vgg':
	num_features = 512

if K.image_dim_ordering() == 'th':
	input_shape_img = (3, None, None)
	input_shape_features = (num_features, None, None)
else:
	input_shape_img = (None, None, 3)
	input_shape_features = (None, None, num_features)


img_input = Input(shape=input_shape_img)
roi_input = Input(shape=(C.num_rois, 4))
feature_map_input = Input(shape=input_shape_features)

# define the base network (resnet here, can be VGG, Inception, etc)
shared_layers = nn.nn_base(img_input, trainable=True)

# define the RPN, built on the base layers
num_anchors = len(C.anchor_box_scales) * len(C.anchor_box_ratios)
rpn_layers = nn.rpn(shared_layers, num_anchors)


classifier = nn.classifier(feature_map_input, roi_input, C.num_rois, nb_classes=len(class_mapping), trainable=True)

model_rpn = Model(img_input, rpn_layers)

model_classifier_only = Model([feature_map_input, roi_input], classifier)


model_classifier = Model([feature_map_input, roi_input], classifier)

print('Loading weights from {}'.format(C.model_path))
model_rpn.load_weights(C.model_path, by_name=True)

model_classifier.load_weights(C.model_path, by_name=True)

model_rpn.compile(optimizer='sgd', loss='mse')

model_classifier.compile(optimizer='sgd', loss='mse')

all_imgs = []

classes = {}

bbox_threshold = 0.0

visualise = True

proposal_count = 0
correct_count = 0

#item_name=['Black box','INTEX','Brown box','FIN','Lemon tea','Black tea','Green tea','Milk tea','Fast noodles(small)','Fast noodles(big)']

item_name=['Lays','Doritos','Enaak','Squid','Calamuju','Potato chips','Cola','Chicken Noodles','Fast noodles(big)','Fast noodles(small)']
def paint_chinese_opencv(im,chinese,pos,color):
	img_PIL = Image.fromarray(cv2.cvtColor(im,cv2.COLOR_BGR2RGB))
	font = ImageFont.truetype('NotoSansCJK-Bold.ttc',25)
	fillColor = color #(255,0,0)
	position = pos #(100,100)
	if not isinstance(chinese,unicode):
		chinese = chinese.decode('utf-8')
	draw = ImageDraw.Draw(img_PIL)
	draw.text(position,chinese,font=font,fill=fillColor)

	img = cv2.cvtColor(np.asarray(img_PIL),cv2.COLOR_RGB2BGR)
	return img

class webcamCapture:
	def __init__(self,cam_num):
		self.Frame = []
		self.capture = cv2.VideoCapture(cam_num)
		self.capture.set(3,1280)
		self.capture.set(4,720)
	def start(self):
	# 把程式放進子執行緒，daemon=True 表示該執行緒會隨著主執行緒關閉而關閉。
		t1=threading.Thread(target=self.queryframe, args=())
		t1.daemon=True
		t1.start()
	def getframe(self):
		# 當有需要影像時，再回傳最新的影像。
		return self.Frame
	def queryframe(self):
		while 1:
			self.status, self.Frame = self.capture.read()
		
webcam = webcamCapture(0)

# 啟動子執行緒
webcam.start()

# 暫停1秒，確保影像已經填充
time.sleep(1)

# 使用無窮迴圈擷取影像，直到按下Esc鍵結束
while True:
#for i in range(0,120):
	#img=cv2.imread('smart_shelf_img/test/data50/data50/'+str(i)+'.jpg')
	time_now=time.time()
	
	# 使用 getframe 取得最新的影像
	img= webcam.getframe()
	
	#img=img[28:667,325:956]

	img[40:623,200:1114]
	
	img = cv2.copyMakeBorder(img, 50, 50, 50, 50, cv2.BORDER_CONSTANT, value=[0,0,0])
	X, ratio = format_img(img, C)

	if K.image_dim_ordering() == 'tf':
		X = np.transpose(X, (0, 2, 3, 1))
	
	# get the feature maps and output from the RPN
	[Y1, Y2, F] = model_rpn.predict(X)
	
	#R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_dim_ordering(), overlap_thresh=0.9, max_boxes=20)#要提出幾個框
	R = roi_helpers.rpn_to_roi(Y1, Y2, C, K.image_dim_ordering(), overlap_thresh=0.7, max_boxes=300)
	# convert from (x1,y1,x2,y2) to (x,y,w,h)
	R[:, 2] -= R[:, 0]
	R[:, 3] -= R[:, 1]
	
	# apply the spatial pyramid pooling to the proposed regions
	bboxes = {}
	bboxes2 = {}
	probs = {}
	for jk in range(R.shape[0]//C.num_rois + 1):
		ROIs = np.expand_dims(R[C.num_rois*jk:C.num_rois*(jk+1), :], axis=0)
		
		if ROIs.shape[1] == 0:
			break
		
		if jk == R.shape[0]//C.num_rois:
			#pad R
			curr_shape = ROIs.shape
			target_shape = (curr_shape[0],C.num_rois,curr_shape[2])
			ROIs_padded = np.zeros(target_shape).astype(ROIs.dtype)
			ROIs_padded[:, :curr_shape[1], :] = ROIs
			ROIs_padded[0, curr_shape[1]:, :] = ROIs[0, 0, :]
			ROIs = ROIs_padded
		
		
		[P_cls, P_regr] = model_classifier_only.predict([F, ROIs])
		for ii in range(P_cls.shape[1]):

			
			if np.max(P_cls[0, ii, :]) < bbox_threshold or np.argmax(P_cls[0, ii, :]) == (P_cls.shape[2] - 1):
				#print(img_name)					
				#print np.argmax(P_cls[0, ii, :])
				continue
			
			cls_name = class_mapping[np.argmax(P_cls[0, ii, :])]

			if cls_name not in bboxes:
				bboxes[cls_name] = []
				probs[cls_name] = []
			if cls_name not in bboxes2:
				bboxes2[cls_name] = []

			(x, y, w, h) = ROIs[0, ii, :]

			cls_num = np.argmax(P_cls[0, ii, :])
			try:
				(tx, ty, tw, th) = P_regr[0, ii, 4*cls_num:4*(cls_num+1)]
				
				tx /= C.classifier_regr_std[0]
				ty /= C.classifier_regr_std[1]
				tw /= C.classifier_regr_std[2]
				th /= C.classifier_regr_std[3]
				x, y, w, h = roi_helpers.apply_regr(x, y, w, h, tx, ty, tw, th)
				
			except:
				pass
			
			probs[cls_name].append(np.max(P_cls[0, ii, :]))
			#print probs
			bboxes[cls_name].append([C.rpn_stride*x, C.rpn_stride*y, C.rpn_stride*(x+w), C.rpn_stride*(y+h)])
		if probs!={}:
			max_probs=max(probs.iteritems(),key=operator.itemgetter(1))[0]
	#print bboxes
	#draw
	all_dets = []
	proposal_count += len(bboxes)
	same_count=0
	for key in bboxes:
		#if max_probs==key and same_count==0:
		if same_count==0:
			same_count=0
			bbox = np.array(bboxes[key])
		
			#print 'key:'+key
			#print 'cat:'+cat
			
			#重疊到0.5就認定兩個是同樣的東西 
			new_boxes, new_probs = roi_helpers.non_max_suppression_fast(bbox, np.array(probs[key]), overlap_thresh=0.3)
		
			for jk in range(new_boxes.shape[0]):
				(x1, y1, x2, y2) = new_boxes[jk,:]

				(real_x1, real_y1, real_x2, real_y2) = get_real_coordinates(ratio, x1, y1, x2, y2)
				#print real_x1,real_y1,real_x2,real_y2
				real_x1=real_x1
				real_y1=real_y1
				real_x2=real_x2
				real_y2=real_y2
				if real_x1<0:
					real_x1=0
				if real_y1<0:
					real_y1=0
				if real_x2>img.shape[1]:
					real_x2=img.shape[1]
				if real_y2>img.shape[0]:
					real_y2=img.shape[0]
				
				
				
				cv2.rectangle(img,(real_x1, real_y1), (real_x2, real_y2), (0, 0, 255), 8)
				
				textLabel = '{}: {}'.format(item_name[int(key)],int(100*new_probs[jk]))
				all_dets.append((key,100*new_probs[jk]))

				(retval,baseLine) = cv2.getTextSize(textLabel,cv2.FONT_HERSHEY_COMPLEX,1,1)
				textOrg = (real_x1, real_y1-0)

				cv2.rectangle(img, (textOrg[0] - 5, textOrg[1]+baseLine - 5), (textOrg[0]+retval[0] + 5, textOrg[1]-retval[1] - 5), (0, 0, 0), 2)
				cv2.rectangle(img, (textOrg[0] - 5,textOrg[1]+baseLine - 5), (textOrg[0]+retval[0] + 5, textOrg[1]-retval[1] - 5), (255, 255, 255), -1)
			
				cv2.putText(img, textLabel, textOrg, cv2.FONT_HERSHEY_DUPLEX, 1, (0, 0, 0), 1)
	recon_time=time.time()
	print(recon_time-time_now)
	#cv2.imwrite('smart_shelf_img/test/data50/result/'+str(i)+'.jpg',img)
	
	cv2.imshow('Image',img)
	if cv2.waitKey(1) & 0xFF == ord('t'):
		print('take picture')
		cv2.imwrite('data/cam0_'+str(time_now)+'.jpg',img)
			

	if cv2.waitKey(1) == 27:
		cv2.destroyAllWindows()
		webcam.stop()
		break
	
