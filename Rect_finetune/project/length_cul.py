#-*- coding: utf-8 -*-
import cv2
import numpy as np 
import os
import csv
import CSV_reader
import statistics 
from scipy import ndimage
from skimage import measure
import time

def get_lenth_width():
    csv_dir = '/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/ground_truth/bbox_via_region_data (16).csv' 
    lenth_list = []
    width_list = []
    width_coordinate = []
    with open(csv_dir,'r') as f:		
        for line in f:			
            filename, x1, y1, x2, y2, class_name = CSV_reader.parse_line2(line)            
            if filename == '0_1.jpg':
                lenth_list.append(x2-x1)#寬
                width_list.append(y2-y1)#深
                width_coordinate.append([y1,y2])
    mean_lenth_list = []
    mean_width_list = []
    width_y_coordinates = []
    for column in range(0,len(lenth_list),4):
        mean_lenth_list.append(statistics.mean(lenth_list[column:column+4]))
        mean_width_list.append(width_list[column:column+4])
        width_y_coordinates.append(width_coordinate[column:column+4])
    print mean_lenth_list
    print mean_width_list
    print width_y_coordinates
    return mean_lenth_list, mean_width_list, width_y_coordinates

def find_rect_lenth(label):
    image, contours, hierarchy = cv2.findContours(label,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # find bounding box coordinates
    coordinate_xs = []
    for cnt in contours:
        x,y,w,h = cv2.boundingRect(cnt)        
        coordinate_xs.append(x)
        coordinate_xs.append(x+w)
    
    return min(coordinate_xs), max(coordinate_xs)

def get_label_x_info(min_x, max_x, mean_lenth_list):
    label_x_info = [0, min_x]
    lenth = max_x - min_x
    # mean_lenth = np.mean(mean_lenth_list)
    total_lenth = sum(mean_lenth_list)
    print 'real:',lenth,'info: ', total_lenth
    # num_row = int(round(total_lenth/mean_lenth))
    zoom_ratio = lenth / total_lenth
    temp = min_x
    for x in range(len(mean_lenth_list)):
        temp += int(lenth*mean_lenth_list[x]/total_lenth)
        label_x_info.append(temp)
    return label_x_info


def seg_row_img(label, label_x_info, mean_width_list, width_y_coordinates):
    seg_labels = []
    seg_label_y_coordinates = []
    for x in range(1,len(label_x_info)-1):
        seg_label = label[0:label.shape[0],label_x_info[x]:label_x_info[x+1]]
        seg_label = check_seg_label_small_area(seg_label)
        bounding_y_coordinates = check_seg_label_row_area(seg_label, mean_width_list[x-1], width_y_coordinates[x-1])
        seg_label_y_coordinates.append(bounding_y_coordinates)
        seg_labels.append(seg_label)
        
    
    # del label_x_info[-1]
    return label, seg_labels, seg_label_y_coordinates
 
def check_seg_label_small_area(seg_label):
    temp = 0
    width = 40 #每次大框滑動的距離
    for x in range(0,seg_label.shape[0], width):
        crop = seg_label[x:x+width,0:seg_label.shape[1]]
        if label_area(crop)/width < seg_label.shape[1]/2:
            seg_label[x:x+width,0:seg_label.shape[1]] = 0
        else:
            seg_label[x:x+width,0:seg_label.shape[1]] = 255
    return seg_label


def label_area(img):
	label, num_features = ndimage.measurements.label(img)
	area_value = 0
	for region in measure.regionprops(label):
			area_value += region.area
	return area_value



def check_seg_label_row_area(seg_label, mean_width, width_y_coordinate):
    bounding_y_coordinates = []
    image, contours, hierarchy = cv2.findContours(seg_label,cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    print 'width_y_coordinate:',width_y_coordinate
    for cnt in contours:
        y_coordinates = []
        x,y,w,h = cv2.boundingRect(cnt)
        print ('x,y:',x,y)
        y_coordinates.append(y)
        min_dis = 1000
        min_dis_num = 0
        product_num = 1
        
        for index, w_y in enumerate(width_y_coordinate):
            if abs(w_y[0]-y) < min_dis:
                min_dis = abs(w_y[0]-y)
                print min_dis
                min_dis_num = index
        n = min_dis_num
        print ('n:',n)
        sum_width = int(mean_width[n]*0.83)
        index = 0
        next_y = y
        #先做參考圖的寬度sum
        if n+1 < len(mean_width):
            while (h - sum_width) > int(mean_width[n+1]*0.83/2) :                 
                print '(h - sum_width):',(h - sum_width),'sum_width:',sum_width,'mean_width[n+1]*0.83:',mean_width[n]*0.83
                n += 1    
                sum_width += int(mean_width[n]*0.83)
                print '(h - sum_width):',(h - sum_width),'sum_width:',sum_width,'mean_width[n+1]*0.83:',mean_width[n]*0.83
                #print ((h - sum_width), mean_width[n])
                product_num += 1
                if n+1 == len(mean_width):
                    break
                print n
                print('---------------------------')            
            #h:real , sum_width:config 
            print 'product num:',product_num
            # print 'h,sum_width,min_dis_num:',h,sum_width,min_dis_num
        for index in range(product_num):
            next_y = next_y + int(mean_width[min_dis_num+index]*0.83*h/sum_width)
            y_coordinates.append(next_y)    
        bounding_y_coordinates.append(y_coordinates)
    # print 'bounding_y_coordinates:',bounding_y_coordinates  
        print('---------------------------')   
    print bounding_y_coordinates
    return bounding_y_coordinates

def lenth_cul_main(filename, frcnn_coordiates):
    mean_lenth_list, mean_width_list, width_y_coordinates = get_lenth_width()
    
    init_label = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/cnn_img/0_1.jpg', 0)
    min_x, max_x = find_rect_lenth(init_label)
    label_x_info = get_label_x_info(min_x, max_x, mean_lenth_list) 
   
    cnn_recongnize_boundings = []

    raw = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/fasterRCNN/test3/'+filename)
    label = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/cnn_img/'+filename, 0)
    # print raw.shape,label.shape
    min_x, max_x = find_rect_lenth(label)    
    label, seg_labels, seg_label_y_coordinates = seg_row_img(label, label_x_info, mean_width_list, width_y_coordinates)
   
    for index_row in range(1,len(label_x_info)-1):
        for index_col in range(len(seg_label_y_coordinates[index_row-1])):
            for index in range(len(seg_label_y_coordinates[index_row-1][index_col])-1):
                
                #cv2.rectangle(raw, (label_x_info[index_row],seg_label_y_coordinates[index_row-1][index_col][index]), (label_x_info[index_row+1], seg_label_y_coordinates[index_row-1][index_col][index+1]), (0, 0, 255), 3)
                cnn_recongnize_boundings.append([label_x_info[index_row],seg_label_y_coordinates[index_row-1][index_col][index],label_x_info[index_row+1], seg_label_y_coordinates[index_row-1][index_col][index+1]])
    # for frcnn_coordiate in frcnn_coordiates:
        
    #     cv2.rectangle(raw, (frcnn_coordiate[0],frcnn_coordiate[1]), (frcnn_coordiate[2],frcnn_coordiate[3]), (0, 255, 0), 3)    
    #cv2.imwrite(filename, raw)    
    return cnn_recongnize_boundings, raw
    


if __name__ == "__main__":
    Result_File = open("CNN_result.csv","w")
    w = csv.writer(Result_File)
    mean_lenth_list, mean_width_list, width_y_coordinates = get_lenth_width()
    path = '/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/cnn_result/'
    folder = os.listdir(path)
    folder.sort()
    init_label = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/cnn_img/0_1.jpg', 0)
    min_x, max_x = find_rect_lenth(init_label)
    label_x_info = get_label_x_info(min_x, max_x, mean_lenth_list) 
    for file in folder:
        cnn_recongnize_boundings = []
        init_time = time.time()
        print file
        raw = cv2.imread('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/data/crop_test/'+file)
        label = cv2.imread(path+file, 0)
        min_x, max_x = find_rect_lenth(label)    
        label, seg_labels, seg_label_y_coordinates = seg_row_img(label, label_x_info, mean_width_list, width_y_coordinates)
        # print label.shape
        # print 'label_x_info:',label_x_info
        # print 'seg_label_y_coordinates:',seg_label_y_coordinates
        for index_row in range(1,len(label_x_info)-1):
            for index_col in range(len(seg_label_y_coordinates[index_row-1])):
                for index in range(len(seg_label_y_coordinates[index_row-1][index_col])-1):
                    print 'index_row:',index_row,label_x_info[index_row],label_x_info[index_row+1]
                    print 'index_col:',index_col,seg_label_y_coordinates[index_row-1][index_col][index]
                    cv2.rectangle(raw, (label_x_info[index_row],seg_label_y_coordinates[index_row-1][index_col][index]), (label_x_info[index_row+1], seg_label_y_coordinates[index_row-1][index_col][index+1]), (0, 0, 255), 3)
                    cnn_recongnize_boundings.append([label_x_info[index_row],seg_label_y_coordinates[index_row-1][index_col][index],label_x_info[index_row+1], seg_label_y_coordinates[index_row-1][index_col][index+1]])
                    data = file, [label_x_info[index_row],seg_label_y_coordinates[index_row-1][index_col][index], label_x_info[index_row+1], seg_label_y_coordinates[index_row-1][index_col][index+1]]
                    w.writerow(data)
        # print cnn_recongnize_boundings
        cv2.imwrite('/home/n100-2/CNN/smart_shelf_product_detect/Rect_finetune/test_result/20190716/'+file,raw)
        now_time = time.time()
        # print now_time-init_time
    

